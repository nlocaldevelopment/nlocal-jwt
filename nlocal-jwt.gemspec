# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'nlocal/jwt/version'

Gem::Specification.new do |spec|
  spec.name          = "nlocal-jwt"
  spec.version       = Nlocal::Jwt::VERSION
  spec.authors       = ["Daniel Prado"]
  spec.email         = ["daniel.prado@nlocal.com"]

  spec.summary       = %q{Encapsultates Nlocal JWT authentication logic}
  spec.description   = %q{Use this in any new microservice}
  spec.homepage      = "https://bitbucket.org/nlocaldevelopment/nlocal-jwt"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "factory_girl", "~> 4.0"
  spec.add_development_dependency "dotenv-rails", "~> 2.1"
  spec.add_development_dependency "byebug", "~> 9.0"
  spec.add_development_dependency "vcr","~> 3.0"
  spec.add_runtime_dependency "rack", "~> 2.0"
  spec.add_runtime_dependency "rails", ">= 5.1.0"
  spec.add_runtime_dependency "warden", "~> 1.2"
  spec.add_runtime_dependency "jwt"
  spec.add_runtime_dependency "virtus"
  spec.add_runtime_dependency "inflecto"
end
