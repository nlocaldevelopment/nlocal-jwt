require "action_controller/metal"
require "rails"

module Nlocal
  module Jwt

   class UnauthorizedController < ActionController::Metal
    include ActionController::UrlFor
    include ActionController::Redirecting
    #include Rails.application.routes.url_helpers
    #include Rails.application.routes.mounted_helpers

    delegate :flash, :to => :request

    def self.call(env)
      @respond ||= action(:respond)
      @respond.call(env)
    end

    def respond
      http_auth
    end

    def http_auth
      self.status = 401
      self.content_type = request.format.to_s
      self.response_body = http_auth_body
    end

    def http_auth_body
      return i18n_message unless request_format
      method = "to_#{request_format}"
      if method == "to_xml"
        { error: i18n_message }.to_xml(root: "errors")
      elsif {}.respond_to?(method)
        { error: i18n_message }.send(method)
      else
        i18n_message
      end
    end

    def request_format
      @request_format ||= request.format.try(:ref)
    end

    protected


    def i18n_message(default = nil)
      message = warden_message || default || "Unauthenticated"
      if message.is_a?(Symbol)
        options = {}
        options[:resource_name] = scope
        options[:scope] = "warden.failure"
        options[:default] = message

        I18n.t(:"#{scope}.#{message}", options)
      else
        message.to_s
      end
    end

    def warden
     request.respond_to?(:get_header) ? request.get_header("warden") : request.env["warden"]
    end

    def warden_message
      @message ||= warden.message || warden_options[:message] || warden_custom[:message]
    end

    def warden_custom
      request.env["warden.custom"] || {}
    end

    def warden_options
      request.respond_to?(:get_header) ? request.get_header("warden.options") : request.env["warden.options"]
    end

    def scope
      @scope ||= warden_options[:scope]
    end

   end

 end
end
