require_relative "./configuration"
require "jwt"

module Nlocal
  module Jwt
    module Auth
     extend self

     def encode(payload, expiration = nil)
       expiration ||= Rails.application.secrets.jwt_expiration_hours

       payload = payload.dup
       payload['exp'] = expiration.to_i.hours.from_now.to_i

       JWT.encode payload, ::Nlocal::Jwt.configuration.secret_key
     end

     def decode(token)
       begin
         decoded_token = JWT.decode token, ::Nlocal::Jwt.configuration.secret_key, true, { :algorithm => 'HS512' }

         decoded_token.first
       rescue
         nil
       end
     end

    end
  end
end
