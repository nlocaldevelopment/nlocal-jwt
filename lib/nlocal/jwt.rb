require "jwt"
require_relative "./jwt/version"
require_relative "./jwt/configuration"
Dir[File.expand_path("../jwt/models/**/*.rb", __FILE__)].each{ |f| require f}

module Nlocal
  module Jwt
    # Your code goes here...
    class << self
      attr_accessor :configuration
    end

    def self.configuration
      @configuration ||= Configuration.new
    end

    def self.reset
      @configuration = Configuration.new
    end

    def self.configure
      yield(configuration)
    end
  end
end

require_relative "./jwt/auth"
Dir[File.expand_path("../jwt/controllers/**/*.rb", __FILE__)].each{ |f| require f}
Dir[File.expand_path("../jwt/warden/**/*.rb", __FILE__)].each{ |f| require f}
require_relative "./jwt/railtie" if defined?(Rails)
