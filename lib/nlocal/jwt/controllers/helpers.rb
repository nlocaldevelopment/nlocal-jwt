module Nlocal
  module Jwt
    module Helpers
      extend ActiveSupport::Concern

      # note: don't specify included or ClassMethods if unused

      included do
         include ActionController::Serialization
      end

      protected

      def warden
        request.env['warden']
      end

      def authenticated?
       !current_user.nil?
      end

      def current_user
        warden.user(:api)
      end

      def authenticate!
        warden.authenticate! scope: :api
      end
   end
  end
end
