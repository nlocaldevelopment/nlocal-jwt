require_relative "./base"
require_relative "./role"

module Nlocal
  module Jwt
   class User < Base
    include Virtus.model(finalize: false)
    attribute :id, Integer
    attribute :email, String
    attribute :username, String
    attribute :type, String
    attribute :roles, Array['Nlocal::Jwt::Role']
   end
 end
end
