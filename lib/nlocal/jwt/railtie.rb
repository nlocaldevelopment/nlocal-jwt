require_relative './warden/strategies/json_web_token'
require_relative './controllers/unauthorized_controller'
require_relative './controllers/helpers'
require "rails"

module Nlocal
  module Jwt
    class Railtie < ::Rails::Railtie
      initializer "nlocal-jwt.configure" do |app|
        puts "Initializing #{self} with warden middleware"

        app.config.middleware.insert_before Rack::Head, Warden::Manager do |manager|
         # Registering your new Strategy
         manager.strategies.add(:jwt, Warden::Strategies::JsonWebToken)
         manager.failure_app = UnauthorizedController

         # Adding the new JWT Strategy to the top of Warden's list,
         # Scoped by what Devise would scope (typically :user)
         manager.default_strategies(scope: :user).unshift :jwt
         manager.scope_defaults(
          :api,
          :strategies => [:jwt],
          :store      => true,
          :action     => "unauthenticated_api"
         )
        end

      end

     initializer "nlocal-jwt.action_controller" do
       ActiveSupport.on_load(:action_controller) do
         puts "Extending #{self} with Nlocal::Jwt::Controller"
         # ActionController::Base gets a method that allows controllers to include the new behavior
         include Nlocal::Jwt::Helpers # ActiveSupport::Concern
       end
     end

     initializer "nlocal-jwt.virtus" do
       Virtus.finalize
     end

    end
  end
end
