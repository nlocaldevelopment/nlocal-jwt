require 'warden'
require_relative '../../auth'
require_relative '../../models/user'

module Warden
  module Strategies
    class JsonWebToken < ::Warden::Strategies::Base
      def valid?
        request.get_header('HTTP_AUTHORIZATION').present?
      end

      def authenticate!
        return fail! unless data = claims
        return fail! unless data && data.has_key?('user') && data['user'].has_key?('id')

        if data.has_key?("exp") && data["exp"].to_i >= Time.now.to_i
          success! Nlocal::Jwt::User.new(data['user'])
        else
          fail!
        end
      end

      protected ######################## PROTECTED #############################

      def claims
        strategy, token = request.get_header('HTTP_AUTHORIZATION').split(' ')
        return nil if (strategy || '').downcase != 'bearer'

        Nlocal::Jwt::Auth.decode(token) rescue nil
      end
    end
  end
end
