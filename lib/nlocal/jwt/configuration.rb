require_relative './warden/strategies/json_web_token'

module Nlocal
  module Jwt
    class Configuration
      attr_accessor  :secret_key

      def initialize
        @secret_key = ENV["JWT_KEY_BASE"]
        Virtus.finalize
      end

      class ClientNotConfigured < Exception; end

    end
  end
end
