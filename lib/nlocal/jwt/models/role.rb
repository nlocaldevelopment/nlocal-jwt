require_relative "./base"
require_relative "./user"

module Nlocal
  module Jwt
   class Role < Base
     include Virtus.model(finalize: false)
     attribute :id, Integer
     attribute :name, String
     attribute :user, 'Nlocal::Jwt::User'
   end
  end
end
